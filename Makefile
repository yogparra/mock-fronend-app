compose-up:
	docker-compose -f docker-compose.yml up -d

app-challenge-up:
	make compose-up	

app-challenge-restart:
	docker stop mock-fronend-app
	docker rm mock-fronend-app
	docker rmi mock-fronend-app
	make compose-up

app-challenge-down:
	docker stop mock-fronend-app
	docker rm mock-fronend-app
	docker rmi mock-fronend-app