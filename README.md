### mock-fronend-app
```
    npm install
    npm run dev
    http://localhost:3000
```

### detalles
```
    node/typescript
    nextjs/react  
    tailwindcss/nextui
```

### docker (windows/linux)
```
    docker build -t mock-fronend-app .
    docker run --name=mock-fronend-app -p 3000:3000 mock-fronend-app
```

### docker-compose (linux)
```
    make app-challenge-up
    make app-challenge-restart
    make app-challenge-down
```