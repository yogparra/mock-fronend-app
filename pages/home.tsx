'use client'

import {Personas} from '@/components/Personas'

export default function homePage() {
    return (    
        <div className="mx-auto max-w-screen-xl px-4 py-16 sm:px-6 lg:px-8">
            <div className="mx-auto max-w-lg">

                <h1 className="text-center text-2xl font-bold text-primary-400 sm:text-3xl">
                Titulo
                </h1>
                
                <p className="mx-auto mt-4 max-w-md text-center text-gray-500">
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Obcaecati sunt
                dolores deleniti inventore quaerat mollitia?
                </p>

                <div className="mb-0 mt-6 space-y-4 rounded-lg p-4 shadow-lg sm:p-6 lg:p-8">                    
                    <Personas />
                </div>
            </div>
        </div>
    )
}