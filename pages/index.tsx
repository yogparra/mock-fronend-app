'use client'

import {Personas} from '@/components/Personas'

export default function indexPage() {
    return (
        <div className="container mx-auto m-10 p-5">
            <div className="p-10 grid grid-cols-1 gap-5">
                <Personas />
            </div>                        
        </div>
    )
  }